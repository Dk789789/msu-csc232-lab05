/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file   EnhancedArrayBagTest.cpp
 * @author Jim Daehn <jdaehn@missouristate.edu>
 * @brief  Implementation of Enhanced Array Bag Unit Test.
 * @see    http://sourceforge.net/projects/cppunit/files
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <iostream>
#include "EnhancedArrayBagTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(EnhancedArrayBagTest);
const std::vector<std::string> EnhancedArrayBagTest::lhdata = {"a", "b", "c"};
const std::vector<std::string> EnhancedArrayBagTest::rhdata = {"b", "b", "d", "e"};
const std::vector<std::string> EnhancedArrayBagTest::expectedUnion{"a", "b", "b", "b", "c", "d", "e"};
const std::vector<std::string> EnhancedArrayBagTest::expectedIntersection{"b"};
const std::vector<std::string> EnhancedArrayBagTest::expectedDifference{"a", "c"};

const int EnhancedArrayBagTest::EXPECTED_UNION_SIZE = EnhancedArrayBagTest::expectedUnion.size();
const int EnhancedArrayBagTest::EXPECTED_INTERSECTION_SIZE = EnhancedArrayBagTest::expectedIntersection.size();
const int EnhancedArrayBagTest::EXPECTED_DIFFERENCE_SIZE = EnhancedArrayBagTest::expectedDifference.size();

/* Function prototype for utility function */
template<class T>
void assertFrequencies(EnhancedArrayBag<T> expected, EnhancedArrayBag<T> actual);

EnhancedArrayBagTest::EnhancedArrayBagTest() {
    // no-op
}

EnhancedArrayBagTest::~EnhancedArrayBagTest() {
    // no-op
}

/**
 * Operation that executes just before each unit test.
 */
void EnhancedArrayBagTest::setUp() {
    for (auto element : lhdata) {
        lhbag.add(element);
    }

    for (auto element : rhdata) {
        rhbag.add(element);
    }
}

/**
 * Operation that executes just after each unit test.
 */
void EnhancedArrayBagTest::tearDown() {
    lhbag.clear();
    rhbag.clear();
}

/**
 * Assert that the results of the union operattion contain the correct frequencies 
 * of each expected item.
 */
void EnhancedArrayBagTest::testUnionResults() {
    EnhancedArrayBag<std::string> expectedUnionBag;
    expectedUnionBag.add("a");
    expectedUnionBag.add("b");
    expectedUnionBag.add("b");
    expectedUnionBag.add("b");
    expectedUnionBag.add("c");
    expectedUnionBag.add("d");
    expectedUnionBag.add("e");
    EnhancedArrayBag<std::string> actualUnionBag{lhbag.unionWithBag(rhbag)};
    assertFrequencies(expectedUnionBag, actualUnionBag);
}

/**
 * Assert that the expected size is as expected to ensure no extraneous information
 * has been added or subtracted from the union.
 */
void EnhancedArrayBagTest::testUnionResultSize() {
    EnhancedArrayBag<std::string> unionBag{lhbag.unionWithBag(rhbag)};
    CPPUNIT_ASSERT_EQUAL_MESSAGE("The size of the actual bag disagrees with the expected size.",
        EnhancedArrayBagTest::EXPECTED_UNION_SIZE, unionBag.getCurrentSize());
}

/**
 * Assert that the results of the intersection operattion contain the correct frequencies 
 * of each expected item.
 */
void EnhancedArrayBagTest::testIntersectionResults() {
    EnhancedArrayBag<std::string> expectedIntersectionBag;
    expectedIntersectionBag.add("b");
    EnhancedArrayBag<std::string> actualIntersectionBag{lhbag.intersectionWithBag(rhbag)};
    assertFrequencies(expectedIntersectionBag, actualIntersectionBag);
}

/**
 * Assert that the expected size is as expected to ensure no extraneous information
 * has been added or subtracted from the intersection.
 */
void EnhancedArrayBagTest::testIntersectionResultSize() {
    EnhancedArrayBag<std::string> intersectionBag{lhbag.intersectionWithBag(rhbag)}; 
    CPPUNIT_ASSERT_EQUAL_MESSAGE("The size of the actual bag disagrees with the expected size.",
        EnhancedArrayBagTest::EXPECTED_INTERSECTION_SIZE, intersectionBag.getCurrentSize());
}

/**
 * Assert that the results of the difference operattion contain the correct frequencies 
 * of each expected item.
 */
void EnhancedArrayBagTest::testDifferenceResults() {
    EnhancedArrayBag<std::string> expectedDifferenceBag;
    expectedDifferenceBag.add("a");
    expectedDifferenceBag.add("c");
    EnhancedArrayBag<std::string> actualDifferenceBag{lhbag.differenceWithBag(rhbag)};
    assertFrequencies(expectedDifferenceBag, actualDifferenceBag);
}

/**
 * Assert that the expected size is as expected to ensure no extraneous information
 * has been added or subtracted from the difference.
 */
void EnhancedArrayBagTest::testDifferenceResultSize() {
    EnhancedArrayBag<std::string> differenceBag{lhbag.differenceWithBag(rhbag)};
    CPPUNIT_ASSERT_EQUAL_MESSAGE("The size of the actual bag disagrees with the expected size.",
        EnhancedArrayBagTest::EXPECTED_DIFFERENCE_SIZE, differenceBag.getCurrentSize());
}

/**
 * Utility function that verifies the expected frequencies of each element.
 *
 * @param expected the expected bag results
 * @param actual   the actual bag results
 */
template<class T>
void assertFrequencies(EnhancedArrayBag<T> expected, EnhancedArrayBag<T> actual) {
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"a\" assertion failed.", expected.getFrequencyOf("a"), actual.getFrequencyOf("a"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"b\" assertion failed.", expected.getFrequencyOf("b"), actual.getFrequencyOf("b"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"c\" assertion failed.", expected.getFrequencyOf("c"), actual.getFrequencyOf("c"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"d\" assertion failed.", expected.getFrequencyOf("d"), actual.getFrequencyOf("d"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"e\" assertion failed.", expected.getFrequencyOf("e"), actual.getFrequencyOf("e"));
}
